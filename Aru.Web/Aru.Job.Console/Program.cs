﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;

namespace Aru.Job.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();

                connBuilder.DataSource = "qiondbserver.database.windows.net";
                connBuilder.UserID = "qiondbuser";
                connBuilder.Password = "Q1onSalesForce2019!!@";
                connBuilder.InitialCatalog = "QionSalesforceDB";

                using (SqlConnection connection = new SqlConnection(connBuilder.ConnectionString))
                {
                    connection.Open();

                    string cmdText = $@"UPDATE [dbo].[AssignmentGroups]
                                        SET [EndTime] = GETUTCDATE()
                                       WHERE [EndTime] is null AND CONVERT(varchar, [StartTime], 23) = '{DateTime.UtcNow.ToString("yyyy-MM-dd", new CultureInfo("en-US"))}'";

                    using (SqlCommand sqlCmd = new SqlCommand(cmdText, connection))
                    {
                        int result = sqlCmd.ExecuteNonQuery();

                        System.Console.WriteLine($"Executing non query : {result}");
                    }
                }
            }
            catch (SqlException e)
            {
                System.Console.WriteLine(e.InnerException);
            }
        }
    }
}
