﻿using Aru.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace Aru.Infrastructure.Services
{
    public class TextFileReader : ITextFileReader
    {
        public IEnumerable<Dictionary<string, object>> Read(string path, string delimiter, int totalColumn)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            
            try
            {
                using(StreamReader reader = new StreamReader(path))
                {
                    string line;
                    int row = 0;
                    string[] columnName = new string[totalColumn];

                    while((line = reader.ReadLine()) != null)
                    {
                        string[] arrayData = line.Split(',');
                        Dictionary<string, object> newItem = new Dictionary<string, object>();
                        if(row == 0)
                        {
                            for(int i = 0; i < totalColumn;i++)
                            {
                                columnName[i] = arrayData[i];
                            }
                        }
                        else
                        {
                            for (int i = 0; i < totalColumn; i++)
                            {
                                newItem.Add(columnName[i], arrayData[i]);
                            }

                            if (newItem != null)
                            {
                                result.Add(newItem);
                            }
                        }
                        row++;
                    }
                    reader.Close();
                }
            }catch(Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
