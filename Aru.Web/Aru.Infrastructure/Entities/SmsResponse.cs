﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aru.Infrastructure.Entities
{
    public class SmsResponse
    {
        public string Id { get; set; }
        public DateTime? DateSent { get; set; }
        public string Status { get; set; }
    }
}
