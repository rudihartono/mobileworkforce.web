﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aru.Infrastructure.Entities
{
    public enum SmsStatus : short
    {

        Queued,
        Sending,
        Sent,
        Failed,
        Delivered,
        Undelivered,
        Receiving,
        Received,
        Accepted,
    }
}
