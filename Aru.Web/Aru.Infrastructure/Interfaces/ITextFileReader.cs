﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aru.Infrastructure.Interface
{
    public interface ITextFileReader
    {
        IEnumerable<Dictionary<string, object>> Read(string path, string delimiter, int totalColumn);
    }
}
