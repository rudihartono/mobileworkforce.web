﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Aru.Infrastructure.GoogleMapsAPI
{
    public interface IGoogleMapsAPIService
    {
        Task<double> GetTravelDistance(LatLng source, LatLng destination);
        Task<int> GetTravelDuration(LatLng source, LatLng destination);
    }
}
