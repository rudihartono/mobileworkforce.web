﻿using System.Threading.Tasks;

namespace Aru.Infrastructure.GoogleMapsAPI
{
    public abstract class GoogleMapsAPI
    {
        protected string _mapKey;
        public GoogleMapsAPI()
        {

        }
        public GoogleMapsAPI(string mapKey)
        {
            _mapKey = mapKey;
        }
        public abstract Task<int> GetTravelDuration(LatLng source, LatLng destination);
        public abstract Task<double> GetTravelDistance(LatLng source, LatLng destination);
    }
}
