﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Aru.Infrastructure.Data.Models
{
    public class CustomerDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerDetailId { get; set; }
        [ForeignKey("Customer")]
        public string CustomerId { get; set; }
        public string BrandName { get; set; }
        public string BrandAddress { get; set; }
        public string BrandCity { get; set; }
        public string BrandDistrict { get; set; }
        public string BrandVillage { get; set; }
        public string BrandStatus { get; set; }
        public string BrandAge { get; set; }
        public string BrandType { get; set; }
        public string BrandingName { get; set; }
        public string Desc { get; set; }
        public string BrandPhotoUrl { get; set; }
        public string BrandPhotoUrl1 { get; set; }
        public double? BrandLatitude { get; set; }
        public double? BrandLongitude { get; set; }

        public string Reserved { get; set; }
        public string BrandPhotoBlobId { get; set; }
        public string brandPhoto1BlobId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
