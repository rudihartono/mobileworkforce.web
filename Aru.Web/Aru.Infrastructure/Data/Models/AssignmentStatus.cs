﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Aru.Infrastructure.Data.Models
{
    public class AssignmentStatus
    {
        [Key]
        [StringLength(20, MinimumLength = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string AssignmentStatusCode { get; set; }

        [Required]
        [StringLength(50)]
        public string AssignmentStatusName { get; set; }

        public string Description { get; set; }
    }
}
