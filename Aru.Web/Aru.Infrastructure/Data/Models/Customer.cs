﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Aru.Infrastructure.Data.Models
{
    public class Customer:BaseData
    {
        [Key]
        [StringLength(50, MinimumLength = 24)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CustomerId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string CustomerEmail { get; set; }
        [Required]
        [StringLength(100)]
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerPhoto { get; set; }
        public double? CustomerLatitude { get; set; }
        public double? CustomerLongitude { get; set; }
        public string Desc { get; set; }
        public string CustomerDistrict { get; set; }
        public string CustomerVillage { get; set; }
        public string CustomerPhotoId { get; set; }
        public string CustomerPhotoNPWP { get; set; }
        public string CustomerPhotoBlobId { get; set; }
        public string CustomerPhotoIdBlobId { get; set; }
        public string CustomerPhotoNPWPBlobId { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }
    }
}
