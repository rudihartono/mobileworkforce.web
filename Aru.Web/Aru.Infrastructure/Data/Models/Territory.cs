﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Aru.Infrastructure.Data.Models
{
	public class Territory : BaseData
	{
		public Territory()
		{
			//this.User = new HashSet<User>();
		}

		[Key]
		[StringLength(50, MinimumLength = 24)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public string TerritoryId { get; set; }
		[StringLength(220)]
		public string Name { get; set; }
		public string Desc { get; set; }

		public virtual ICollection<UserTerritory> UserTerritory { get; set; }

		//[NotMapped]
		//public virtual ICollection<User> User { get; set; }
	}
}
