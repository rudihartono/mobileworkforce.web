﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class UpdatePaymentAndAssignmentDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BlobName",
                table: "Payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Attachment",
                table: "AssignmentDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentBlobId",
                table: "AssignmentDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BlobName",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "Attachment",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "AttachmentBlobId",
                table: "AssignmentDetails");
        }
    }
}
