﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class userterritory1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Territories_Users_UserId",
                table: "Territories");

            migrationBuilder.DropIndex(
                name: "IX_Territories_UserId",
                table: "Territories");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Territories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Territories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Territories_UserId",
                table: "Territories",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Territories_Users_UserId",
                table: "Territories",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
