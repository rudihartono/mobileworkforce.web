﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class UpdateCustomerDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerDetails_Customers_CustomerId",
                table: "CustomerDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CustomerDetails",
                table: "CustomerDetails");

            migrationBuilder.RenameTable(
                name: "CustomerDetails",
                newName: "CustomerDetail");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerDetails_CustomerId",
                table: "CustomerDetail",
                newName: "IX_CustomerDetail_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CustomerDetail",
                table: "CustomerDetail",
                column: "CustomerDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerDetail_Customers_CustomerId",
                table: "CustomerDetail",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerDetail_Customers_CustomerId",
                table: "CustomerDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CustomerDetail",
                table: "CustomerDetail");

            migrationBuilder.RenameTable(
                name: "CustomerDetail",
                newName: "CustomerDetails");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerDetail_CustomerId",
                table: "CustomerDetails",
                newName: "IX_CustomerDetails_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CustomerDetails",
                table: "CustomerDetails",
                column: "CustomerDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerDetails_Customers_CustomerId",
                table: "CustomerDetails",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
