﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class AddTableCustomerContactAgent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomerContactAgents",
                columns: table => new
                {
                    CustomerContactAgentId = table.Column<string>(maxLength: 50, nullable: false),
                    ContactId = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    SalesId = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerContactAgents", x => x.CustomerContactAgentId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerContactAgents");
        }
    }
}
