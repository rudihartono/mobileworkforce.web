﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class AddUserCodeToDetailAssignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Reserved1",
                table: "AssignmentGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reserved2",
                table: "AssignmentGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserCode",
                table: "AssignmentDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Reserved1",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "Reserved2",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "UserCode",
                table: "AssignmentDetails");
        }
    }
}
