﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class renameColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Surveys_ServeyId",
                table: "Answers");

            migrationBuilder.RenameColumn(
                name: "ServeyId",
                table: "Answers",
                newName: "SurveyId");

            migrationBuilder.RenameIndex(
                name: "IX_Answers_ServeyId",
                table: "Answers",
                newName: "IX_Answers_SurveyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Surveys_SurveyId",
                table: "Answers",
                column: "SurveyId",
                principalTable: "Surveys",
                principalColumn: "SurveyId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Surveys_SurveyId",
                table: "Answers");

            migrationBuilder.RenameColumn(
                name: "SurveyId",
                table: "Answers",
                newName: "ServeyId");

            migrationBuilder.RenameIndex(
                name: "IX_Answers_SurveyId",
                table: "Answers",
                newName: "IX_Answers_ServeyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Surveys_ServeyId",
                table: "Answers",
                column: "ServeyId",
                principalTable: "Surveys",
                principalColumn: "SurveyId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
