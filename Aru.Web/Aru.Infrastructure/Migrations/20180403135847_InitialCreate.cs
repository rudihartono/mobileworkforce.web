﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    AccountId = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    CurrentToken = table.Column<string>(nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    LastLoginDt = table.Column<DateTime>(nullable: false),
                    Password = table.Column<string>(maxLength: 64, nullable: false),
                    Role = table.Column<string>(maxLength: 20, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.AccountId);
                });

            migrationBuilder.CreateTable(
                name: "AssignmentStatuses",
                columns: table => new
                {
                    AssignmentStatusCode = table.Column<string>(maxLength: 20, nullable: false),
                    AssignmentStatusName = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentStatuses", x => x.AssignmentStatusCode);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerId = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    CustomerAddress = table.Column<string>(nullable: true),
                    CustomerCity = table.Column<string>(nullable: true),
                    CustomerEmail = table.Column<string>(nullable: false),
                    CustomerLatitude = table.Column<decimal>(nullable: true),
                    CustomerLongitude = table.Column<decimal>(nullable: true),
                    CustomerName = table.Column<string>(maxLength: 30, nullable: false),
                    CustomerPhoto = table.Column<string>(nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerId);
                });

            migrationBuilder.CreateTable(
                name: "GetAssignmentGroups",
                columns: table => new
                {
                    AssignmentGroupId = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    EndDistance = table.Column<double>(nullable: false),
                    EndLatitude = table.Column<decimal>(nullable: true),
                    EndLongitude = table.Column<decimal>(nullable: true),
                    EndTime = table.Column<DateTime>(nullable: false),
                    StartDistance = table.Column<double>(nullable: false),
                    StartLatitude = table.Column<decimal>(nullable: true),
                    StartLongitude = table.Column<decimal>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GetAssignmentGroups", x => x.AssignmentGroupId);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    ProductCode = table.Column<string>(maxLength: 30, nullable: true),
                    ProductImage = table.Column<string>(maxLength: 150, nullable: true),
                    ProductName = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleId = table.Column<string>(nullable: false),
                    RoleCode = table.Column<string>(nullable: true),
                    RoleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "Surveys",
                columns: table => new
                {
                    SurveyId = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: true),
                    SurveyLink = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surveys", x => x.SurveyId);
                });

            migrationBuilder.CreateTable(
                name: "UserLocation",
                columns: table => new
                {
                    UserLocationId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    Latitude = table.Column<decimal>(nullable: false),
                    Longitude = table.Column<decimal>(nullable: false),
                    Reserved1 = table.Column<string>(nullable: true),
                    Reserved2 = table.Column<string>(nullable: true),
                    UserCode = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLocation", x => x.UserLocationId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<string>(maxLength: 50, nullable: false),
                    AccountId = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true),
                    UserCode = table.Column<string>(maxLength: 20, nullable: true),
                    UserEmail = table.Column<string>(maxLength: 50, nullable: true),
                    UserName = table.Column<string>(maxLength: 20, nullable: true),
                    UserPhone = table.Column<string>(maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Assignments",
                columns: table => new
                {
                    AssignmentId = table.Column<string>(maxLength: 50, nullable: false),
                    AssignmentCode = table.Column<string>(nullable: true),
                    AssignmentStatusCode = table.Column<string>(nullable: false),
                    AssignmentName = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    Latitude = table.Column<decimal>(nullable: true),
                    Longitude = table.Column<decimal>(nullable: true),
                    Remarks = table.Column<string>(maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assignments", x => x.AssignmentId);
                    table.ForeignKey(
                        name: "FK_Assignments_AssignmentStatuses_AssignmentStatusCode",
                        column: x => x.AssignmentStatusCode,
                        principalTable: "AssignmentStatuses",
                        principalColumn: "AssignmentStatusCode",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    ContactId = table.Column<string>(maxLength: 50, nullable: false),
                    ContactName = table.Column<string>(maxLength: 40, nullable: true),
                    ContactNumber = table.Column<string>(maxLength: 15, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<string>(maxLength: 50, nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.ContactId);
                    table.ForeignKey(
                        name: "FK_Contacts_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    AnswerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssignmentCode = table.Column<string>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    ServeyId = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.AnswerId);
                    table.ForeignKey(
                        name: "FK_Answers_Surveys_ServeyId",
                        column: x => x.ServeyId,
                        principalTable: "Surveys",
                        principalColumn: "SurveyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssignmentDetails",
                columns: table => new
                {
                    AssignmentDetailId = table.Column<string>(maxLength: 50, nullable: false),
                    AssignmentId = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentDetails", x => x.AssignmentDetailId);
                    table.ForeignKey(
                        name: "FK_AssignmentDetails_Assignments_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assignments",
                        principalColumn: "AssignmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssignmentDetails_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    InvoiceId = table.Column<string>(maxLength: 50, nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    AssignmentCode = table.Column<string>(nullable: true),
                    AssignmentId = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    InvoiceCode = table.Column<string>(nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.InvoiceId);
                    table.ForeignKey(
                        name: "FK_Invoices_Assignments_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assignments",
                        principalColumn: "AssignmentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    OrderId = table.Column<string>(maxLength: 50, nullable: false),
                    AssignmentCode = table.Column<string>(nullable: true),
                    AssignmentId = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    ProductAmount = table.Column<decimal>(nullable: false),
                    ProductCode = table.Column<string>(nullable: true),
                    ProductName = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_Orders_Assignments_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assignments",
                        principalColumn: "AssignmentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    PaymentId = table.Column<string>(maxLength: 50, nullable: false),
                    AssignmentCode = table.Column<string>(nullable: true),
                    AssignmentId = table.Column<string>(maxLength: 50, nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    GiroName = table.Column<string>(nullable: true),
                    GrioPhotoUrl = table.Column<string>(nullable: true),
                    InvoiceCode = table.Column<string>(nullable: true),
                    IsActive = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<byte>(nullable: false),
                    PaymentAmount = table.Column<decimal>(nullable: false),
                    PaymentChannel = table.Column<int>(nullable: false),
                    PaymentCredit = table.Column<decimal>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedDt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.PaymentId);
                    table.ForeignKey(
                        name: "FK_Payments_Assignments_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assignments",
                        principalColumn: "AssignmentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_ServeyId",
                table: "Answers",
                column: "ServeyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentDetails_AssignmentId",
                table: "AssignmentDetails",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentDetails_UserId",
                table: "AssignmentDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Assignments_AssignmentStatusCode",
                table: "Assignments",
                column: "AssignmentStatusCode");

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_CustomerId",
                table: "Contacts",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_AssignmentId",
                table: "Invoices",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_AssignmentId",
                table: "Orders",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_AssignmentId",
                table: "Payments",
                column: "AssignmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "AssignmentDetails");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "GetAssignmentGroups");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "UserLocation");

            migrationBuilder.DropTable(
                name: "Surveys");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Assignments");

            migrationBuilder.DropTable(
                name: "AssignmentStatuses");
        }
    }
}
