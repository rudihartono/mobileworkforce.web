﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class renameTableAssignmentGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GetAssignmentGroups",
                table: "GetAssignmentGroups");

            migrationBuilder.RenameTable(
                name: "GetAssignmentGroups",
                newName: "AssignmentGroups");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AssignmentGroups",
                table: "AssignmentGroups",
                column: "AssignmentGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AssignmentGroups",
                table: "AssignmentGroups");

            migrationBuilder.RenameTable(
                name: "AssignmentGroups",
                newName: "GetAssignmentGroups");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GetAssignmentGroups",
                table: "GetAssignmentGroups",
                column: "AssignmentGroupId");
        }
    }
}
