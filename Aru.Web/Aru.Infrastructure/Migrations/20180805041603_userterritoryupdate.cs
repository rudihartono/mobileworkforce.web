﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class userterritoryupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTerritories_Territories_TerritoryId",
                table: "UserTerritories");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTerritories_Users_UserId",
                table: "UserTerritories");

            migrationBuilder.DropIndex(
                name: "IX_UserTerritories_TerritoryId",
                table: "UserTerritories");

            migrationBuilder.DropIndex(
                name: "IX_UserTerritories_UserId",
                table: "UserTerritories");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "UserTerritories",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TerritoryId",
                table: "UserTerritories",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Territories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Territories_UserId",
                table: "Territories",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Territories_Users_UserId",
                table: "Territories",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Territories_Users_UserId",
                table: "Territories");

            migrationBuilder.DropIndex(
                name: "IX_Territories_UserId",
                table: "Territories");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Territories");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "UserTerritories",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TerritoryId",
                table: "UserTerritories",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserTerritories_TerritoryId",
                table: "UserTerritories",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTerritories_UserId",
                table: "UserTerritories",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTerritories_Territories_TerritoryId",
                table: "UserTerritories",
                column: "TerritoryId",
                principalTable: "Territories",
                principalColumn: "TerritoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTerritories_Users_UserId",
                table: "UserTerritories",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
