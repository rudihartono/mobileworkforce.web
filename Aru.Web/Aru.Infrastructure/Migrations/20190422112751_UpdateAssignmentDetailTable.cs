﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class UpdateAssignmentDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GoogleTime",
                table: "AssignmentDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsVerified",
                table: "AssignmentDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SalesTime",
                table: "AssignmentDetails",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GoogleTime",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "IsVerified",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "SalesTime",
                table: "AssignmentDetails");
        }
    }
}
