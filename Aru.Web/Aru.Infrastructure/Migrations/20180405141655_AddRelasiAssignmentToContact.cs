﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class AddRelasiAssignmentToContact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactId",
                table: "AssignmentDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentDetails_ContactId",
                table: "AssignmentDetails",
                column: "ContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentDetails_Contacts_ContactId",
                table: "AssignmentDetails",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "ContactId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentDetails_Contacts_ContactId",
                table: "AssignmentDetails");

            migrationBuilder.DropIndex(
                name: "IX_AssignmentDetails_ContactId",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "AssignmentDetails");
        }
    }
}
