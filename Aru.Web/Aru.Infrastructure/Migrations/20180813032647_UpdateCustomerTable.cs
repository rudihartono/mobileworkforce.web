﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class UpdateCustomerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustomerDistrict",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerPhotoBlobId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerPhotoId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerPhotoIdBlobId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerPhotoNPWP",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerPhotoNPWPBlobId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerVillage",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Desc",
                table: "Customers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerDistrict",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerPhotoBlobId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerPhotoId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerPhotoIdBlobId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerPhotoNPWP",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerPhotoNPWPBlobId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerVillage",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Desc",
                table: "Customers");
        }
    }
}
