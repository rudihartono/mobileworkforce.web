﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class AddFieldAttachmentAssignmentGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Reserved4",
                table: "AssignmentGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reserved5",
                table: "AssignmentGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reserved6",
                table: "AssignmentGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reserved7",
                table: "AssignmentGroups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Attachment1",
                table: "AssignmentDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Attachment2",
                table: "AssignmentDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentBlobId1",
                table: "AssignmentDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentBlobId2",
                table: "AssignmentDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LostTime",
                table: "AssignmentDetails",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Reserved4",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "Reserved5",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "Reserved6",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "Reserved7",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "Attachment1",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "Attachment2",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "AttachmentBlobId1",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "AttachmentBlobId2",
                table: "AssignmentDetails");

            migrationBuilder.DropColumn(
                name: "LostTime",
                table: "AssignmentDetails");
        }
    }
}
