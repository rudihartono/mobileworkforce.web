﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class AddCustomerDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomerDetails",
                columns: table => new
                {
                    CustomerDetailId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BrandAddress = table.Column<string>(nullable: true),
                    BrandAge = table.Column<string>(nullable: true),
                    BrandCity = table.Column<string>(nullable: true),
                    BrandDistrict = table.Column<string>(nullable: true),
                    BrandLatitude = table.Column<double>(nullable: true),
                    BrandLongitude = table.Column<double>(nullable: true),
                    BrandName = table.Column<string>(nullable: true),
                    BrandPhotoBlobId = table.Column<string>(nullable: true),
                    BrandPhotoUrl = table.Column<string>(nullable: true),
                    BrandPhotoUrl1 = table.Column<string>(nullable: true),
                    BrandStatus = table.Column<string>(nullable: true),
                    BrandType = table.Column<string>(nullable: true),
                    BrandVillage = table.Column<string>(nullable: true),
                    BrandingName = table.Column<string>(nullable: true),
                    CustomerId = table.Column<string>(nullable: true),
                    Desc = table.Column<string>(nullable: true),
                    Reserved = table.Column<string>(nullable: true),
                    brandPhoto1BlobId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerDetails", x => x.CustomerDetailId);
                    table.ForeignKey(
                        name: "FK_CustomerDetails_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerDetails_CustomerId",
                table: "CustomerDetails",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerDetails");
        }
    }
}
