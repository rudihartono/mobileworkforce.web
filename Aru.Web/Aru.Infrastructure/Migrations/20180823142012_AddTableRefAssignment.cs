﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class AddTableRefAssignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RefAssigmentCode",
                table: "Assignments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RefAssignmentCode",
                table: "Assignments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RefAssigments",
                columns: table => new
                {
                    Code = table.Column<string>(maxLength: 6, nullable: false),
                    Desc = table.Column<string>(maxLength: 255, nullable: true),
                    GroupId = table.Column<string>(maxLength: 3, nullable: true),
                    IsDeleted = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefAssigments", x => x.Code);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Assignments_RefAssigmentCode",
                table: "Assignments",
                column: "RefAssigmentCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_RefAssigments_RefAssigmentCode",
                table: "Assignments",
                column: "RefAssigmentCode",
                principalTable: "RefAssigments",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_RefAssigments_RefAssigmentCode",
                table: "Assignments");

            migrationBuilder.DropTable(
                name: "RefAssigments");

            migrationBuilder.DropIndex(
                name: "IX_Assignments_RefAssigmentCode",
                table: "Assignments");

            migrationBuilder.DropColumn(
                name: "RefAssigmentCode",
                table: "Assignments");

            migrationBuilder.DropColumn(
                name: "RefAssignmentCode",
                table: "Assignments");
        }
    }
}
