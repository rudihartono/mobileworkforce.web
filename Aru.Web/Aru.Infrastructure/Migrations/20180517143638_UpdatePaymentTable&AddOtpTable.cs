﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Aru.Infrastructure.Migrations
{
    public partial class UpdatePaymentTableAddOtpTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PaymentCredit",
                table: "Payments",
                newName: "TransferAmount");

            migrationBuilder.RenameColumn(
                name: "GrioPhotoUrl",
                table: "Payments",
                newName: "TransferBank");

            migrationBuilder.RenameColumn(
                name: "GiroName",
                table: "Payments",
                newName: "GiroPhoto");

            migrationBuilder.RenameColumn(
                name: "BankName",
                table: "Payments",
                newName: "GiroNumber");

            migrationBuilder.AddColumn<decimal>(
                name: "CashAmount",
                table: "Payments",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GiroAmount",
                table: "Payments",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "InvoiceAmount",
                table: "Payments",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PaymentDebt",
                table: "Payments",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AssignmentGroups",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateTable(
                name: "Otps",
                columns: table => new
                {
                    OtpId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: false),
                    CreatedDt = table.Column<DateTime>(nullable: false),
                    IsValid = table.Column<bool>(nullable: false),
                    ItemId = table.Column<string>(maxLength: 50, nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Value = table.Column<string>(maxLength: 8, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Otps", x => x.OtpId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentGroups_CreatedBy",
                table: "AssignmentGroups",
                column: "CreatedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentGroups_Users_CreatedBy",
                table: "AssignmentGroups",
                column: "CreatedBy",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentGroups_Users_CreatedBy",
                table: "AssignmentGroups");

            migrationBuilder.DropTable(
                name: "Otps");

            migrationBuilder.DropIndex(
                name: "IX_AssignmentGroups_CreatedBy",
                table: "AssignmentGroups");

            migrationBuilder.DropColumn(
                name: "CashAmount",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "GiroAmount",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "InvoiceAmount",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "PaymentDebt",
                table: "Payments");

            migrationBuilder.RenameColumn(
                name: "TransferBank",
                table: "Payments",
                newName: "GrioPhotoUrl");

            migrationBuilder.RenameColumn(
                name: "TransferAmount",
                table: "Payments",
                newName: "PaymentCredit");

            migrationBuilder.RenameColumn(
                name: "GiroPhoto",
                table: "Payments",
                newName: "GiroName");

            migrationBuilder.RenameColumn(
                name: "GiroNumber",
                table: "Payments",
                newName: "BankName");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "AssignmentGroups",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
