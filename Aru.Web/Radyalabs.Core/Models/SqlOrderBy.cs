﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radyalabs.Core.Models
{
    public class SqlOrderBy
    {
        public string Column { get; set; }

        public string Type { get; set; }
    }
}
