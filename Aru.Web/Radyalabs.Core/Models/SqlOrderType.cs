﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radyalabs.Core.Models
{
    public class SqlOrderType
    {
        public static readonly string Ascending = "asc";
        public static readonly string Descending = "desc";
    }
}
