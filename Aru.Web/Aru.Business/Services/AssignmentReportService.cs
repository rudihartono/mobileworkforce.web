﻿using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Aru.Infrastructure.Data;
using Aru.Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using Radyalabs.Core.Interfaces;
using Radyalabs.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aru.Business.Services
{
    public class AssignmentReportService : IAssignmentReportService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly MobileForceContext _dbContext;
        public AssignmentReportService(MobileForceContext context)
        {
            unitOfWork = new UnitOfWork<MobileForceContext>(context);
            _dbContext = context;
        }

        public Task<int> GetAssingmentReportLength(
            string[] regions, string search, DateTime date, string role = "ALL")
        {
            if (string.IsNullOrEmpty(search))
                search = string.Empty;

            IQueryable<string> queryCount = null;
            if(role == "DRIVER")
            {
                queryCount = (from a in _dbContext.AssignmentGroups
                              join b in _dbContext.Users on a.CreatedBy equals b.UserId
                              where a.StartTime.Date == date.Date &&
                              (b.UserCode.Contains(search) || b.UserName.Contains(search))
                              && b.Account.RoleCode == role
                               && regions.Intersect(b.UserTerritory.Select(x => x.TerritoryId).ToArray()).Any()
                              orderby a.StartTime ascending
                              select a.AssignmentGroupId).AsQueryable();
            }
            else
            {
                queryCount = (from a in _dbContext.AssignmentGroups
                              join b in _dbContext.Users on a.CreatedBy equals b.UserId
                              where a.StartTime.Date == date.Date &&
                              (b.UserCode.Contains(search) || b.UserName.Contains(search))
                              && regions.Intersect(b.UserTerritory.Select(x => x.TerritoryId).ToArray()).Any()
                              orderby a.StartTime ascending
                              select a.AssignmentGroupId).AsQueryable();
            }

            return queryCount.CountAsync();
        }

        public async Task<CmsDetailUserReportViewModel> GetDetail(string id, DateTime date)
        {
            var repoUser = unitOfWork.GetRepository<User>();
            repoUser.Condition = PredicateBuilder.True<User>().And(x => x.UserId == id && x.IsDeleted == 0 && x.IsActive == 1);

            var user = repoUser.Find().FirstOrDefault();
            if (user == null)
                throw new ArgumentNullException("User ID", "Not found");

            var result = (from a in _dbContext.AssignmentGroups
                          join b in _dbContext.Users on a.CreatedBy equals b.UserId
                          join c in _dbContext.Accounts.Include(x => x.Role) on b.AccountId equals c.AccountId
                          where b.UserId == id && a.StartTime.AddHours(7).Date == date.Date
                          select new CmsDetailUserReportViewModel
                          {
                              UserCode = b.UserCode,
                              UserId = b.UserId,
                              Name = b.UserName,
                              UserEmail = b.UserEmail,
                              RoleName = c.Role.RoleName,
                              UserName = b.UserName,
                              UserPhone = b.UserPhone,
                              StartLatitude = a.StartLatitude ?? 0,
                              EndLatitude = a.EndLatitude ?? 0,
                              StartLongitude = a.StartLongitude ?? 0,
                              EndLongitude = a.EndLongitude ?? 0,
                              StartTime = a.StartTime,
                              EndedTime = a.EndTime,
                              TotalLossTime = a.TotalLostTime,
                              TotalTaskCompleted = (from x in _dbContext.AssignmentDetails
                                                    where x.UserId == b.UserId
                                                    && x.Assignment.AssignmentStatusCode == "TASK_COMPLETED"
                                                    && (x.Assignment.AssignmentDate.Date == a.StartTime.Date)
                                                    select x.AssignmentDetailId).Count(),
                              VisitHistory = (from y in _dbContext.Assignments
                                              where y.AssignmentDetail.UserId == b.UserId
                                              && a.StartTime.Date == y.AssignmentDate.Date
                                              orderby y.AssignmentDetail.EndTime descending
                                              select new CmsVisitHistoryViewModel
                                              {
                                                  CustomerName = y.AssignmentName,
                                                  Address = y.Address,
                                                  EndedTime = y.AssignmentDetail.EndTime,
                                                  StartedTime = y.AssignmentDetail.StartTime,
                                                  VisitStatus = y.AssignmentStatus.AssignmentStatusCode,
                                                  TaskName = y.AssignmentName,
                                                  TotalInvoice = y.Invoices.Count,
                                                  InvoiceAmount = y.Invoices.Sum(x => x.Amount),
                                                  PaymentAmount = y.Payments.Sum(x => x.CashAmount + x.TransferAmount + x.GiroAmount),
                                                  IsVerified = y.AssignmentDetail.IsVerified,
                                                  GoogleTime = y.AssignmentDetail.GoogleTime,
                                                  SalesTime = y.AssignmentDetail.SalesTime
                                              }
                                              ).ToList(),
                              LocationHistory = (from h in _dbContext.UserActivities
                                                 where h.Assignment.AssignmentDetail.UserId == b.UserId &&
                                                 h.AssignmentStatusCode == ReportConstant.AGENT_STARTED &&
                                                 h.Assignment.AssignmentDate.Date == a.StartTime.Date
                                                 orderby h.CreatedDt ascending
                                                 select new CmsLocationHistoryViewModel
                                                 {
                                                     Label = h.Assignment.AssignmentDetail.Contact.ContactName + ": " + h.Assignment.Address,
                                                     Latitude = h.Latitude,
                                                     Status = h.AssignmentStatusCode,
                                                     Longitude = h.Longitude,
                                                     StartTime = h.CreatedDt,
                                                     GoogleTime = h.Assignment.AssignmentDetail.GoogleTime,
                                                     SalesTime = h.Assignment.AssignmentDetail.SalesTime
                                                 }
                                              ).ToList()
                          }).AsQueryable();

            return await result.FirstOrDefaultAsync();
        }

        public Task<List<CmsReportAssignmentViewModel>> GetListAssignmentReport(
            string[] regions,
            string search, 
            DateTime startDate,
            int length,
            int start,
            string orderBy,
            string orderType,
            string role = "ALL")
        {
            if (string.IsNullOrEmpty(search))
                search = string.Empty;

            IQueryable<CmsReportAssignmentViewModel> queryReport = null;
            if (role == "DRIVER"){
                queryReport = (from a in _dbContext.AssignmentGroups
                               join b in _dbContext.Users on a.CreatedBy equals b.UserId
                               where a.StartTime.Date == startDate.Date
                               && (b.UserName.Contains(search) || b.UserCode.Contains(search))
                               && regions.Intersect(b.UserTerritory.Select(x => x.TerritoryId).ToArray()).Any()
                               && b.Account.RoleCode == "DRIVER"
                               orderby a.StartTime ascending
                               select new CmsReportAssignmentViewModel
                               {
                                   UserCode = b.UserCode,
                                   UserId = b.UserId,
                                   Name = b.UserName,
                                   StartTime = a.StartTime,
                                   EndedTime = a.EndTime,
                                   UserName = b.UserName,
                                   TotalLossTime = a.TotalLostTime,
                                   TotalTaskCompleted = (from c in _dbContext.AssignmentDetails
                                                         where c.UserId == b.UserId
                                                         && c.Assignment.AssignmentStatusCode == ReportConstant.TASK_COMPLETED
                                                         //&& (c.EndTime != null && c.EndTime.Value.Date == a.StartTime.Date)
                                                         && (c.Assignment.AssignmentDate.Date == a.StartTime.Date)
                                                         select c.AssignmentDetailId).Count(),
                                   CmsTotalKunjungan = (from d in _dbContext.AssignmentDetails
                                                        where d.UserId == b.UserId
                                                        && (d.Assignment.AssignmentDate.Date == a.StartTime.Date)
                                                        select d.AssignmentId).Count(),
                                   CmsTotalPayment = (from e in _dbContext.Assignments
                                                      where e.AssignmentDetail.UserId == b.UserId
                                                      && (e.AssignmentDate.Date == a.StartTime.Date)
                                                      select e.Payments.Sum(x => x.CashAmount + x.GiroAmount + x.TransferAmount)).Sum(),
                                   ListVisit = (from f in _dbContext.Assignments
                                                where f.AssignmentDetail.UserId == b.UserId
                                                && (f.AssignmentDate.Date == a.StartTime.Date)
                                                select new CmsVisitDetail
                                                {
                                                    StartTime = f.AssignmentDetail.StartTime,
                                                    EndTime = f.AssignmentDetail.EndTime
                                                }).ToList(),
                               }).Skip(start).Take(length).AsQueryable();
            }
            else {
                queryReport = (from a in _dbContext.AssignmentGroups
                               join b in _dbContext.Users on a.CreatedBy equals b.UserId
                               where a.StartTime.Date == startDate.Date
                               && (b.UserName.Contains(search) || b.UserCode.Contains(search))
                               && regions.Intersect(b.UserTerritory.Select(x => x.TerritoryId).ToArray()).Any()
                               orderby a.StartTime ascending
                               select new CmsReportAssignmentViewModel
                               {
                                   UserCode = b.UserCode,
                                   UserId = b.UserId,
                                   Name = b.UserName,
                                   StartTime = a.StartTime,
                                   EndedTime = a.EndTime,
                                   UserName = b.UserName,
                                   TotalLossTime = a.TotalLostTime,
                                   TotalTaskCompleted = (from c in _dbContext.AssignmentDetails
                                                         where c.UserId == b.UserId
                                                         && c.Assignment.AssignmentStatusCode == ReportConstant.TASK_COMPLETED
                                                         //&& (c.EndTime != null && c.EndTime.Value.Date == a.StartTime.Date)
                                                         && (c.Assignment.AssignmentDate.Date == a.StartTime.Date)
                                                         select c.AssignmentDetailId).Count(),
                                   CmsTotalKunjungan = (from d in _dbContext.AssignmentDetails
                                                        where d.UserId == b.UserId
                                                        && (d.Assignment.AssignmentDate.Date == a.StartTime.Date)
                                                        select d.AssignmentId).Count(),
                                   CmsTotalPayment = (from e in _dbContext.Assignments
                                                      where e.AssignmentDetail.UserId == b.UserId
                                                      && (e.AssignmentDate.Date == a.StartTime.Date)
                                                      select e.Payments.Sum(x => x.CashAmount + x.GiroAmount + x.TransferAmount)).Sum(),
                                   ListVisit = (from f in _dbContext.Assignments
                                                where f.AssignmentDetail.UserId == b.UserId
                                                && (f.AssignmentDate.Date == a.StartTime.Date)
                                                select new CmsVisitDetail
                                                {
                                                    StartTime = f.AssignmentDetail.StartTime,
                                                    EndTime = f.AssignmentDetail.EndTime
                                                }).ToList(),
                               }).Skip(start).Take(length).AsQueryable();
            }
            
            return queryReport.ToListAsync();
        }
    }
}
