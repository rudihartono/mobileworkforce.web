﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aru.Business.Entities
{
    public enum AttendanceStatus:short
    {
        ABSENT,
        CHECKIN,
        CHECKOUT
    }
}
