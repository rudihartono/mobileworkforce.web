﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aru.Business.Entities
{
    public class AssignmentStatusModel
    {
        public string AssignmentStatusCode { get; set; }
        public string AssignmentStatusName { get; set; }
        public string Description { get; set; }
    }
}
