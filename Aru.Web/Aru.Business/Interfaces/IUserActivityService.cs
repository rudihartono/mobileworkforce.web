﻿using Aru.Business.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Aru.Business.Interfaces
{
    public interface IUserActivityService
    {
        void Add(UserActivityModel userAcivity);
        Task<List<UserActivityModel>> GetUserActivities();
        Task<List<UserActivityModel>> GetUserActivities(string userId);
    }
}
