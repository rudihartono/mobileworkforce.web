﻿using Aru.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Api.Interfaces
{
    public interface IRefService
    {
        IEnumerable<ReasonViewModel> GetReasons();
    }
}
