﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Helpers;
using Aru.Api.Model;
using Aru.Api.ViewModels;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Aru.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/survey")]
    public class SurveyController : Controller
    {
        private readonly IAccountService accountService;
        private readonly ISurveyService surveyService;
        private readonly ApplicationSetting applicationSetting;
        private Radyalabs.Core.Models.JsonEntity json;

        public SurveyController(
            IAccountService accountService,
            ISurveyService surveyService,
            IOptions<ApplicationSetting> options)
        {
            this.accountService = accountService;
            this.surveyService = surveyService;
            applicationSetting = options.Value;
            json = new Radyalabs.Core.Models.JsonEntity();
        }
        
        [HttpPost]
        public IActionResult Post([FromHeader(Name = "X-Aru-Token")]string authKey, AnswerSurveyViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                    if (userAuth == null)
                        return Json(UnAuthorizeResponse.UnauthorizeResponse());

                    AnswerModel surveyAnswer = new AnswerModel();
                    surveyAnswer.SurveyId = model.SurveyId;
                    surveyAnswer.AssignmentCode = model.AssignmentCode;
                    surveyAnswer.AssignmentId = model.AssignmentId;
                    surveyAnswer.UserId = userAuth.UserId;
                    surveyAnswer.AnswerDate = model.CreatedDate;

                    surveyService.Answer(surveyAnswer, userAuth.UserId);

                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.OK, "Success");
                }
                catch(Exception ex)
                {
                    json.SetError(true);
                    json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            else
            {
                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, "Not acceptable");
                json.AddData(ModelState);
            }

            return Json(json);
        }
    }
}