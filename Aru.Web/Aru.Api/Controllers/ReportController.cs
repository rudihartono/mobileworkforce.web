﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Helpers;
using Aru.Api.Interfaces;
using Aru.Api.Model;
using Aru.Api.ViewModels;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Aru.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Report")]
    public class ReportController : Controller
    {
        private Radyalabs.Core.Models.JsonEntity json;
        private readonly ApplicationSetting applicationSetting;
        private readonly IUserReportService reportService;
        private readonly IAccountService accountService;

        public ReportController(IOptions<ApplicationSetting> options, IUserReportService userReportService, IAccountService accountService)
        {
            reportService = userReportService;
            this.accountService = accountService;
            applicationSetting = options.Value;
            json = new Radyalabs.Core.Models.JsonEntity();
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ReportAssignmentViewModel>), 200)]
        public IActionResult Get([FromHeader(Name = "X-Aru-Token")] string authKey, DateTime date, int limit = 10, int offset = 0)
        {
            var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);

            if (userAuth == null)
                return Json(UnAuthorizeResponse.UnauthorizeResponse());

            try
            {
                if (userAuth.RoleCode != "SPV")
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.NotAcceptable, "Method not allowed");
                    json.AddData(null);

                    return Json(json);
                }
                List<string> users = new List<string> { userAuth.UserId };

                var agent = accountService.GetUserAgent("SALES", userAuth.TerritoryId.Distinct().ToList());
                if (agent != null)
                    users.AddRange(agent.Select(x => x.UserId).ToList());

                var reportAssignment = reportService.GetListAssignmentReport(users, date.ToUniversalTime(), limit, offset);
                
                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.OK, "Success");
                json.AddData(reportAssignment);

            }catch(ApplicationException appEx)
            {
                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, appEx.ToString());
                json.AddData(null);
            }
            catch (Exception ex)
            {
                json.SetError(true);
                json.AddAlert((int)HttpStatusCode.InternalServerError, ex.ToString());
                json.AddData(null);
            }

            return Json(json);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DetailUserReportViewModel), 200)]
        public IActionResult Get([FromHeader(Name = "X-Aru-Token")] string authKey, [FromRoute]string id, DateTime date)
        {
            var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);

            if (userAuth == null)
                return Json(UnAuthorizeResponse.UnauthorizeResponse());

            try
            {
                if (userAuth.RoleCode != "SPV")
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.NotAcceptable, "Method not allowed");
                    json.AddData(null);

                    return Json(json);
                }

                var result = reportService.GetDetailAssignmentReport(id, date.ToUniversalTime());

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.OK, "Success");
                json.AddData(result);
            }
            catch (ApplicationException appEx)
            {
                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, appEx.ToString());
                json.AddData(null);
            }
            catch (Exception ex)
            {
                json.SetError(true);
                json.AddAlert((int)HttpStatusCode.InternalServerError, ex.ToString());
                json.AddData(null);
            }

            return Json(json);
        }
    }
}