﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Helpers;
using Aru.Api.Interfaces;
using Aru.Api.Model;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Aru.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/dashborad")]
    public class DashboradController : Controller
    {
        private Radyalabs.Core.Models.JsonEntity json;
        private readonly ApplicationSetting applicationSetting;
        private readonly IUserReportService reportService;
        private readonly IAccountService accountService;
        
        public DashboradController(IOptions<ApplicationSetting> options, IUserReportService userReportService, IAccountService accountService)
        {
            json = new Radyalabs.Core.Models.JsonEntity();
            applicationSetting = options.Value;
            reportService = userReportService;
            this.accountService = accountService;
        }

        [HttpGet]
        public IActionResult Get([FromHeader(Name = "X-Aru-Token")] string authKey, DateTime date)
        {
            var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);

            if (userAuth == null)
                return Json(UnAuthorizeResponse.UnauthorizeResponse());

            try
            {
                string partnerId = string.Empty;
                if (userAuth.RoleCode == "SPV")
                    partnerId = accountService.GetCheckinPartner(userAuth.UserId, DateTime.UtcNow);

                var result = reportService.GetDashbord(userAuth.UserId, date.ToUniversalTime());
                if (result != null)
                {
                    var reportAssignment = reportService.GetListAssignmentReport(new List<string> { userAuth.UserId, partnerId }, date.ToUniversalTime());
                    if(reportAssignment!=null && reportAssignment.Count > 0)
                    {
                        var currentUserReport = reportAssignment.FirstOrDefault(x => x.UserId.Equals(userAuth.UserId));
                        if (currentUserReport != null)
                        {
                            result.TotalWorkTime = currentUserReport.TotalWorkTime == 0 ? Math.Round(DateTime.UtcNow.Subtract(currentUserReport.StartTime.Value).TotalHours, 2) : currentUserReport.TotalWorkTime;
                            result.TotalLostTime = currentUserReport.TotalLossTime;

                            reportAssignment.Remove(currentUserReport);
                        }
                    }

                    result.AssignmentReport = reportAssignment;
                }

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.OK, "Success");
                json.AddData(result);
            }catch(Exception ex)
            {
                json.SetError(true);
                json.AddAlert((int)HttpStatusCode.InternalServerError, ex.ToString());
                json.AddData(null);
            }

            return Json(json);
        }
    }
}