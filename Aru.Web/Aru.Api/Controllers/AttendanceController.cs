﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Helpers;
using Aru.Api.Model;
using Aru.Api.ViewModels;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Aru.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Aru.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Attendance")]
    public class AttendanceController : Controller
    {
        private readonly IAssignmentService assignmentService;
        private readonly IAccountService accountService;
        private readonly IAzureStorageService azureStorageService;
        private readonly ApplicationSetting applicationSetting;
        private Radyalabs.Core.Models.JsonEntity json;
        public AttendanceController(IAssignmentService assignmentService, IAccountService accountService, IAzureStorageService azureStorageService,IOptions<ApplicationSetting> options)
        {
            this.assignmentService = assignmentService;
            this.accountService = accountService;
            this.azureStorageService = azureStorageService;
            applicationSetting = options.Value;
            json = new Radyalabs.Core.Models.JsonEntity();
        }

        [HttpPost]
        [Route("check")]
        public IActionResult Check([FromHeader(Name = "X-Aru-Token")] string authKey)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                    if (userAuth == null)
                        return Json(UnAuthorizeResponse.UnauthorizeResponse());

                    short result = (short)accountService.Check(userAuth.UserId, DateTime.UtcNow.AddHours(7));

                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.OK, "success");
                    json.AddData(result);
                }
                catch (ApplicationException appEx)
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.NotAcceptable, appEx.Message);
                    json.AddData(null);
                }
                catch (Exception ex)
                {
                    json.SetError(true);
                    json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
                    json.AddData(null);
                }
            }
            else
            {
                var messageError = ErrorHelper.Error(ModelState);

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, "");
                json.AddData(messageError);
            }
            return Json(json);
        }

        [HttpPost]
        [Route("checkin")]
        public async Task<IActionResult> Checkin([FromHeader(Name = "X-Aru-Token")] string authKey, [FromForm] CheckinViewModel checkinViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                    if (userAuth == null)
                        return Json(UnAuthorizeResponse.UnauthorizeResponse());

                    DateTime utcNow = DateTime.UtcNow;
                    var result = accountService.Check(userAuth.UserId, 
                        (checkinViewModel.Mode == CheckInMode.OFFLINE? checkinViewModel.StartTime.ToUniversalTime() : utcNow.AddHours(7)));
                    if(result == AttendanceStatus.CHECKIN)
                    {
                        json.SetError(false);
                        json.AddAlert((int)HttpStatusCode.Accepted, "already checked in.");
                        json.AddData(null);

                        return Json(json);
                    }

                    if (userAuth.RoleCode == "DRIVER")
                    {
                        string attachmentId = string.Empty;
                        string attachmentUrl = string.Empty;

                        if (checkinViewModel.Attachment != null)
                        {
                            string attachmentContentType = checkinViewModel.Attachment.ContentType.ToLower();
                            if (attachmentContentType != "image/png" && attachmentContentType != "image/jpg" && attachmentContentType != "image/jpeg")
                            {
                                if (checkinViewModel.Attachment == null)
                                {
                                    json = new Radyalabs.Core.Models.JsonEntity()
                                    {
                                        Error = false,
                                        Data = false,
                                        Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.BadRequest, Message = "Cannot accept attachment file type." }
                                    };

                                    return Json(json);
                                }
                            }

                            if (checkinViewModel.Attachment.Length > 10000000)
                            {

                                json = new Radyalabs.Core.Models.JsonEntity()
                                {
                                    Error = false,
                                    Data = false,
                                    Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.BadRequest, Message = "Attachment exceeds the maximum limit." }
                                };

                                return Json(json);
                            }

                            try
                            {
                                attachmentId = Guid.NewGuid().ToString();
                                attachmentUrl = await azureStorageService.UploadAsync(attachmentId, checkinViewModel.Attachment.OpenReadStream(), checkinViewModel.Attachment.ContentType);
                            }
                            catch
                            {
                                throw new Exception("Gagal mengirimkan gambar");
                            }
                        }

                        accountService.CheckIn(userAuth.UserId, 
                            checkinViewModel.Latitude, 
                            checkinViewModel.Longitude, 
                            (checkinViewModel.Mode == CheckInMode.OFFLINE ? checkinViewModel.StartTime.ToUniversalTime() : utcNow), 
                            checkinViewModel.Comment, "", 
                            checkinViewModel.Distance, 
                            attachmentId, 
                            attachmentUrl);
                    }
                    else if (userAuth.RoleCode == "SPV")
                    {
                        accountService.CheckIn(userAuth.UserId,
                            checkinViewModel.Latitude,
                            checkinViewModel.Longitude,
                            (checkinViewModel.Mode == CheckInMode.OFFLINE ? checkinViewModel.StartTime.ToUniversalTime() : DateTime.UtcNow),
                            checkinViewModel.Comment,
                            checkinViewModel.UserId);

                        var taskCopyTask = assignmentService.CopyTask(checkinViewModel.UserId, userAuth.UserId, utcNow);

                        taskCopyTask.Wait();
                    }
                    else
                        accountService.CheckIn(userAuth.UserId, checkinViewModel.Latitude, checkinViewModel.Longitude, (checkinViewModel.Mode == CheckInMode.OFFLINE ? checkinViewModel.StartTime.ToUniversalTime() : DateTime.UtcNow), checkinViewModel.Comment);

                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.OK, "success");
                    json.AddData(null);
                }
                catch (ApplicationException appEx)
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.NotAcceptable, appEx.Message);
                    json.AddData(null);
                }
                catch (Exception ex)
                {
                    json.SetError(true);
                    json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
                    json.AddData(null);
                }
            }
            else
            {
                var messageError = ErrorHelper.Error(ModelState);

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, "");
                json.AddData(messageError);
            }
            return Json(json);
        }

        [HttpPost]
        [Route("checkout")]
        public async Task<IActionResult> Checkout([FromHeader(Name = "X-Aru-Token")] string authKey, [FromForm] CheckoutViewModel checkoutViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                    if (userAuth == null)
                        return Json(UnAuthorizeResponse.UnauthorizeResponse());

                    var result = accountService.Check(userAuth.UserId, (checkoutViewModel.Mode == CheckInMode.OFFLINE ? 
                        checkoutViewModel.EndTime.ToUniversalTime() : DateTime.UtcNow.AddHours(7)));
                    if (result == AttendanceStatus.CHECKOUT)
                    {
                        json.SetError(false);
                        json.AddAlert((int)HttpStatusCode.Accepted, "already checked out.");
                        json.AddData(null);
    
                        return Json(json);
                    }
                    #region Attachment
                    string attachmentId = string.Empty;
                    string attachmentUrl = string.Empty;

                    if (checkoutViewModel.Attachment != null)
                    {
                        string attachmentContentType = checkoutViewModel.Attachment.ContentType.ToLower();
                        if (attachmentContentType != "image/png" && attachmentContentType != "image/jpg" && attachmentContentType != "image/jpeg")
                        {
                            if (checkoutViewModel.Attachment == null)
                            {
                                json = new Radyalabs.Core.Models.JsonEntity()
                                {
                                    Error = false,
                                    Data = false,
                                    Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.BadRequest, Message = "Cannot accept attachment file type." }
                                };

                                return Json(json);
                            }
                        }

                        if (checkoutViewModel.Attachment.Length > 10000000)
                        {

                            json = new Radyalabs.Core.Models.JsonEntity()
                            {
                                Error = false,
                                Data = false,
                                Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.BadRequest, Message = "Attachment exceeds the maximum limit." }
                            };

                            return Json(json);
                        }

                        try
                        {
                            attachmentId = Guid.NewGuid().ToString();
                            attachmentUrl = await azureStorageService.UploadAsync(attachmentId, checkoutViewModel.Attachment.OpenReadStream(), checkoutViewModel.Attachment.ContentType);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    #endregion

                    accountService.CheckOut(userAuth.UserId, checkoutViewModel.Latitude, checkoutViewModel.Longitude, (checkoutViewModel.Mode == CheckInMode.OFFLINE ? checkoutViewModel.EndTime.ToUniversalTime() : DateTime.UtcNow), checkoutViewModel.Comment, "", checkoutViewModel.Distance, attachmentId, attachmentUrl);

                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.OK, "success");
                    json.AddData(null);
                }
                catch (ApplicationException appEx)
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.NotAcceptable, appEx.Message);
                    json.AddData(null);
                }
                catch (Exception ex)
                {
                    json.SetError(true);
                    json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
                    json.AddData(null);
                }
            }
            else
            {
                var messageError = ErrorHelper.Error(ModelState);

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, "");
                json.AddData(messageError);
            }
            return Json(json);
        }
    }
}