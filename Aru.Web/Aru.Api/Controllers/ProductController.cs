﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Helpers;
using Aru.Api.Model;
using Aru.Api.ViewModels;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace Aru.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/product")]
    public class ProductController : Controller
    {
        private readonly IProductService productService;
        private readonly IAccountService accountService;
        private readonly ApplicationSetting applicationSetting;
        private Radyalabs.Core.Models.JsonEntity json;
        public ProductController(IAccountService accountService, IProductService productService, IOptions<ApplicationSetting> options)
        {
            this.productService = productService;
            this.accountService = accountService;
            this.applicationSetting = options.Value;
            json = new Radyalabs.Core.Models.JsonEntity();
        }
        
        [HttpGet]
        public ActionResult Get([FromHeader(Name = "X-Aru-Token")]string authKey)
        {
            try
            {
                var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                if (userAuth == null)
                    return Json(UnAuthorizeResponse.UnauthorizeResponse());

                int totalRow = productService.TotalRow();
                var result = productService.Get("", totalRow, 0);

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.OK, "Success");
                json.AddData(result);
            }
            catch (Exception ex)
            {
                json.SetError(true);
                json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
            }
            return Json(json);
        }

        [HttpPost]
        [Route("order")]
        public async Task<ActionResult> Order([FromHeader(Name = "X-Aru-Token")]string authKey, [FromBody]OrderViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                if (userAuth == null)
                    return Json(UnAuthorizeResponse.UnauthorizeResponse());

                if (viewModel.Products != null && viewModel.Products.Count > 0)
                {
                    try
                    {
                        List<OrderModel> orderModels = new List<OrderModel>();
                        foreach (var item in viewModel.Products)
                        {
                            orderModels.Add(new OrderModel
                            {
                                AssignmentId = viewModel.AssignmentId,
                                CustomerId = viewModel.CustomerId,
                                OrderId = Guid.NewGuid().ToString(),
                                ProductCode = item.ProductCode,
                                Discount = item.Discount,
                                ProductAmount = item.ProductAmount,
                                ProductName = item.ProductName,
                                Quantity = item.Quantity
                            });
                        }

                        await productService.Order(orderModels, userAuth.UserId);

                        json.SetError(false);
                        json.AddAlert((int)HttpStatusCode.OK, "Success");
                        json.AddData(null);
                    }
                    catch(Exception ex)
                    {
                        json.SetError(false);
                        json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
                        json.AddData(ex);
                    }
                }
                else
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.NotAcceptable, "product is empty");
                    json.AddData(ModelState);
                }
            }
            else
            {
                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, "Not acceptable");
                json.AddData(ModelState);
            }

            return Json(json);
        }
    }
}