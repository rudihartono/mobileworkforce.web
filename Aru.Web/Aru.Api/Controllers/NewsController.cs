﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Model;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Aru.Business.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Aru.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/news")]
    public class NewsController : Controller
    {
        private readonly IContentManagementService contentManagementService;
        private readonly ApplicationSetting appSetting;
        private Radyalabs.Core.Models.JsonEntity json;
        public NewsController(IContentManagementService contentManagementService,
            IOptions<ApplicationSetting> options)
        {
            json = new Radyalabs.Core.Models.JsonEntity();
            this.appSetting = options.Value;
            this.contentManagementService = contentManagementService;
        }
        
        [HttpGet]
        public IActionResult Get(string keyword = "", int limit = 10, int offset = 0)
        {
            List<NewsModel> news = new List<NewsModel>();
            try
            {
                news = contentManagementService.Get(keyword, limit, offset, "PublishedDate", "desc", true);
                json.AddAlert((int)HttpStatusCode.OK, "success");
                json.AddData(news);
            }
            catch(Exception ex)
            {
                json.AddAlert((int)HttpStatusCode.InternalServerError, ex.ToString());
                json.AddData(null);
            }

            return Json(json);
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            NewsModel news = new NewsModel();
            try
            {
                news = contentManagementService.Get(id);

                json.AddAlert((int)HttpStatusCode.OK, "success");
                json.AddData(news);
            }
            catch (Exception ex)
            {
                json.AddAlert((int)HttpStatusCode.InternalServerError, ex.ToString());
                json.AddData(null);
            }

            return Json(json);
        }
    }
}