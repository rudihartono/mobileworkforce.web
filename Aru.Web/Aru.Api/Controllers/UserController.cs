﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aru.Api.Helpers;
using Aru.Api.Model;
using Aru.Api.ViewModels;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Radyalabs.Core.Helpers;

namespace Aru.Api.Controllers
{   
    [Produces("application/json")]
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly IAccountService accountService;
        private readonly IUserActivityService activityService;
        private readonly ApplicationSetting applicationSetting;
        private Radyalabs.Core.Models.JsonEntity json;
        public UserController(
            IAccountService accountService,
            IUserActivityService activityService,
            IOptions<ApplicationSetting> options)
        {
            this.accountService = accountService;
            this.activityService = activityService;
            this.applicationSetting = options.Value;
            json = new Radyalabs.Core.Models.JsonEntity();
        }

        [HttpGet]
        [Route("agent")]
        [ProducesResponseType(typeof(IEnumerable<UserAgentModel>), 200)]
        public IActionResult Agent([FromHeader(Name = "X-Aru-Token")] string authKey, string role = "SALES")
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                    if (userAuth == null)
                        return Json(UnAuthorizeResponse.UnauthorizeResponse());


                    var listAgent = accountService.GetUserAgent(role, userAuth.TerritoryId);

                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.OK, "Success");
                    json.AddData(listAgent);
                }
                catch (Exception ex)
                {
                    json.SetError(false);
                    json.AddAlert((int)HttpStatusCode.InternalServerError, ex.Message);
                    json.AddData(null);
                }
            }
            else
            {
                var messageError = ErrorHelper.Error(ModelState);

                json.SetError(false);
                json.AddAlert((int)HttpStatusCode.NotAcceptable, "");
                json.AddData(messageError);
            }

            return Json(json);
        }

        [HttpPost]
        [Route("activity")]
        public IActionResult LocationUpdate([FromHeader(Name = "X-Aru-Token")] string authKey, [FromBody]UpdateLocationViewModel locationViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = ApiAuthHelper.Get<UserAuthenticated>(authKey, applicationSetting.EncryptionKey, applicationSetting.VerificationKey);
                    if (userAuth == null)
                        return Json(UnAuthorizeResponse.UnauthorizeResponse());

                    if(locationViewModel.CreatedDt == null)
                    {
                        locationViewModel.CreatedDt = DateTime.UtcNow;
                    }
                    else
                    {
                        locationViewModel.CreatedDt = locationViewModel.CreatedDt.Value.ToUniversalTime();
                    }

                    UserActivityModel activityModel = new UserActivityModel();
                    CopyProperty.CopyPropertiesTo(locationViewModel, activityModel);
                    activityModel.CreatedBy = userAuth.UserId;
                    

                    activityService.Add(activityModel);
                    json = new Radyalabs.Core.Models.JsonEntity()
                    {
                        Error = false,
                        Data = null,
                        Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.OK, Message = "Success" }
                    };

                }
                catch (Exception ex)
                {
                    json = new Radyalabs.Core.Models.JsonEntity()
                    {
                        Error = true,
                        Data = null,
                        Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.InternalServerError, Message = ex.Message }
                    };
                }
            }
            else
            {
                json = new Radyalabs.Core.Models.JsonEntity()
                {
                    Error = false,
                    Data = ErrorHelper.Error(ModelState),
                    Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.InternalServerError, Message = "NotAcceptable" }
                };
            }

            return Json(json);
        }
    }
}