﻿using Aru.Api.Interfaces;
using Aru.Api.ViewModels;
using Aru.Infrastructure.Data;
using Aru.Infrastructure.Data.Models;
using Radyalabs.Core.Interfaces;
using Radyalabs.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Api.Services
{
    public class RefService:IRefService
    {
        private IUnitOfWork unitOfWork;
        private IRepository<RefAssigment> repoRef;

        public RefService(MobileForceContext context)
        {
            unitOfWork = new UnitOfWork<MobileForceContext>(context);
            repoRef = this.unitOfWork.GetRepository<RefAssigment>();
        }

        public IEnumerable<ReasonViewModel> GetReasons()
        {
            try
            {
                repoRef.Condition = PredicateBuilder.True<RefAssigment>().And(x => x.IsDeleted == 0);
                var result = repoRef.Find<ReasonViewModel>(x => new ReasonViewModel
                {
                    Code = x.Code,
                    Name = x.Name
                }).ToList();

                return result;
            }catch(ApplicationException appEx)
            {
                throw appEx;
            }catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
