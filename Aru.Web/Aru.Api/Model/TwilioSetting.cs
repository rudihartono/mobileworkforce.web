﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Api.Model
{
    public class TwilioSetting
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Sender { get; set; }
    }
}
