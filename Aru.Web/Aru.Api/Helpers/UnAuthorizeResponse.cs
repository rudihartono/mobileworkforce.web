﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Aru.Api.Helpers
{
    public class UnAuthorizeResponse
    {
        public static Radyalabs.Core.Models.JsonEntity UnauthorizeResponse()
        {
            var json = new Radyalabs.Core.Models.JsonEntity()
            {
                Error = false,
                Data = null,
                Alerts = new Radyalabs.Core.Models.JsonAlert { Code = (int)HttpStatusCode.Unauthorized, Message = "Unauthorized" }
            };

            return json;
        }
    }
}
