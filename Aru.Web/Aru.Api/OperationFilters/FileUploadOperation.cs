﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Api.OperationFilters
{
    public class FileUploadOperation : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.OperationId == "ApiAssignmentByAssignment_idCompletePost")
            {
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "file"));
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "file",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });
            }
            else if (operation.OperationId == "ApiAssignmentCreatePost")
            {
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "Attachment"));
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "Attachment",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "Attachment1",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "Attachment2",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });
            }
            else if (operation.OperationId == "ApiAssignmentCalendarByFromByToGet")
            {
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "to"));
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "to",
                    In = "path",
                    Format = "date-time",
                    Description = "to",
                    Required = false,
                    Type = "string"
                });
            }
            else if (operation.OperationId == "ApiInvoicePaymentPost")
            {
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "GiroPhotoAttachment"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "GiroPhotoAttachment1"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "GiroPhotoAttachment2"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "GiroPhotoAttachment3"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "GiroPhotoAttachment4"));

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "GiroPhotoAttachment",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "GiroPhotoAttachment1",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "GiroPhotoAttachment2",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "GiroPhotoAttachment3",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "GiroPhotoAttachment4",
                    In = "formData",
                    Description = "Upload file",
                    Required = false,
                    Type = "file"
                });
            }
            else if (operation.OperationId == "ApiCustomerAddPost")
            {
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "PhotoIdCard"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "PhotoNPWP"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "BrandingPhoto"));
                operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "StorePhoto"));

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "PhotoIdCard",
                    In = "formData",
                    Description = "Foto KTP",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "PhotoNPWP",
                    In = "formData",
                    Description = "Foto NPWP",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "BrandingPhoto",
                    In = "formData",
                    Description = "Foto Plang Toko",
                    Required = false,
                    Type = "file"
                });

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "StorePhoto",
                    In = "formData",
                    Description = "Foto Toko",
                    Required = false,
                    Type = "file"
                });

            }
            else if (operation.OperationId == "ApiAttendanceCheckinPost")
            {
                FillAttachmentParameter(operation);
            }
            else if (operation.OperationId == "ApiAttendanceCheckoutPost")
            {
                FillAttachmentParameter(operation);
            }
        }

        void FillAttachmentParameter(Operation operation)
        {
            operation.Parameters.Remove(operation.Parameters.FirstOrDefault(x => x.Name == "Attachment"));

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "Attachment",
                In = "formData",
                Description = "Upload file",
                Required = false,
                Type = "file"
            });
        }
    }
}
