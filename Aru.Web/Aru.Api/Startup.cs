﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aru.Api.Interfaces;
using Aru.Api.Model;
using Aru.Api.OperationFilters;
using Aru.Api.Services;
using Aru.Business.Interfaces;
using Aru.Business.Services;
using Aru.Infrastructure.Interfaces;
using Aru.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Radyalabs.Core.Interfaces;
using Radyalabs.Core.Repository;
using Swashbuckle.AspNetCore.Swagger;

namespace Aru.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<Aru.Infrastructure.Data.MobileForceContext>(options =>
                options
                .UseSqlServer(Configuration.GetConnectionString("MobileForceAzureConn"),
                sqlServerOptionsAction => sqlServerOptionsAction.EnableRetryOnFailure()
                .CommandTimeout(45)
                .UseRowNumberForPaging(true)));

            services.Configure<ApplicationSetting>(Configuration.GetSection("ApplicationSetting"));
            services.Configure<TwilioSetting>(Configuration.GetSection("TwilioSetting"));

            services.AddScoped<DbContext, Aru.Infrastructure.Data.MobileForceContext>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAssignmentService, AssignmentService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IUserActivityService, UserActivityService>();
            services.AddScoped<ISurveyService, SurveyService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IContentManagementService, ContentManagementService>();
            services.AddScoped<IUserReportService, UserReportService>();
            services.AddScoped<IRefService, RefService>();
            services.AddSingleton<ISequencerNumberService, SequencerNumberService>();

            services.AddScoped<Aru.Infrastructure.GoogleMapsAPI.GoogleMapsAPI, Infrastructure.GoogleMapsAPI.GoogleMapsAPIService>( 
                factory => new Infrastructure.GoogleMapsAPI.GoogleMapsAPIService(Configuration.GetSection("ApplicationSetting")["GoogleMapsKey"], "json"));

            services.AddScoped<IAzureStorageService>(factory =>
            {
                return new AzureStorageService(
                    Configuration.GetSection("AzureBlobSetings")["StorageAccount"],
                    Configuration.GetSection("AzureBlobSetings")["StorageKey"],
                    Configuration.GetSection("AzureBlobSetings")["ContainerName"]);
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Mobile Sales Force", Version = "v1" });
            });

            services.ConfigureSwaggerGen(options =>
                
                options.OperationFilter<FileUploadOperation>()
            );

            services.AddMvc();
            services.AddAuthorization();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mobile Sales Force API v1");
            });

            app.UseMvc();
        }
    }
}
