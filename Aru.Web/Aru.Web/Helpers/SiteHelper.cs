﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.Helpers
{
    public class SiteHelper
    {
        public static string GetBaseUrl(HttpRequest context)
        {
            return $"{context.Scheme}://{context.Host}{context.PathBase}";
        }

        public static string GetCurrentUrl(HttpRequest context)
        {
            return $"{context.Scheme}://{context.Host}{context.PathBase}{context.Path}";
        }
    }
}
