﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.ViewModels
{
    public class UserTerritoryViewModel
    {
		public int UserTerritoryId { get; set; }
		public string UserId { get; set; }
		public string TerritoryId { get; set; }
	}
}
