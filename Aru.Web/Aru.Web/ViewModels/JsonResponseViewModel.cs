﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.ViewModels
{
    public class JsonResponseViewModel
    {
        public bool success { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
