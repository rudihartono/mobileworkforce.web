﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.Models
{
    public class ApplicationSetting
    {
        public string ProjectIdentityName { get; set; }
        public string GoogleMapsKey { get; set; }
    }
}
