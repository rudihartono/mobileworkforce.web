﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aru.Business.Interfaces;
using Aru.Business.Services;
using Aru.Infrastructure.Interface;
using Aru.Infrastructure.Services;
using Aru.Web.Interfaces;
using Aru.Web.Models;
using Aru.Web.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Radyalabs.Core.Interfaces;
using Radyalabs.Core.Repository;

namespace Aru.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Aru.Infrastructure.Data.MobileForceContext>(options =>
                options
                .UseSqlServer(Configuration.GetConnectionString("MobileForceAzureConn"), 
                sqlServerOptionsAction => sqlServerOptionsAction.EnableRetryOnFailure()
                .CommandTimeout(45)
                .UseRowNumberForPaging())
            );
            
            services.AddScoped<DbContext, Aru.Infrastructure.Data.MobileForceContext>();
            services.AddScoped<IAssignmentService, AssignmentService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserManagementService, UserManagementService>();
            services.AddScoped<ITaskReportService, TaskReportService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IDashboardService, DashboardService>();
            services.AddSingleton<ITextFileReader, TextFileReader>();
            services.AddSingleton<ISequencerNumberService, SequencerNumberService>();
            services.AddScoped<ISurveyService, SurveyService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IAssignmentReportService, AssignmentReportService>();
            services.AddScoped<IContentManagementService, ContentManagementService>();
			services.AddScoped<ITerritoryService, TerritoryService>();
            services.Configure<ApplicationSetting>(Configuration.GetSection("ApplicationSetting"));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.AccessDeniedPath = "/Home/AccessDenied";
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logout";
            });

            services.AddAuthorization();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options=> 
                options.SerializerSettings.DateFormatString = "dd/MM/yyyy"
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            
            app.UseStaticFiles();
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            Rotativa.AspNetCore.RotativaConfiguration.Setup(env);
        }
    }
}
