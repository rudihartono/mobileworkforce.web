﻿using Aru.Infrastructure.Data;
using Aru.Infrastructure.Data.Models;
using Aru.Web.Interfaces;
using Aru.Web.ViewModels;
using Microsoft.EntityFrameworkCore;
using Radyalabs.Core.Interfaces;
using Radyalabs.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.Services
{
    public class DashboardService : IDashboardService
    {

        private IUnitOfWork unitOfWork;
        private MobileForceContext dbContext;

        public DashboardService(MobileForceContext mobileForceContext)
        {
            dbContext = mobileForceContext;
            unitOfWork = new UnitOfWork<MobileForceContext>(dbContext);
        }
        public List<LocationHistoryViewModel> GetLocationAgent()
        {
            try
            {
                var result = (from a in dbContext.Users
                              join b in dbContext.Accounts.Include(x=>x.Role) on a.AccountId equals b.AccountId
                              where a.IsActive == 1 && a.IsDeleted == 0 && b.RoleCode != "SA"
                              select new LocationHistoryViewModel
                              {
                                  Label = a.UserName + "("+ b.Role.RoleName + ")",
                                  Latitude = (from c in dbContext.AssignmentGroups
                                              where a.UserId == c.CreatedBy
                                              orderby c.CreatedDt descending
                                              select c.StartLatitude).FirstOrDefault(),
                                  Longitude = (from d in dbContext.AssignmentGroups
                                               where a.UserId == d.CreatedBy
                                               orderby d.CreatedDt descending
                                               select d.StartLongitude).FirstOrDefault(),
                                  StartTime = (from e in dbContext.AssignmentGroups
                                               where a.UserId == e.CreatedBy
                                               orderby e.CreatedDt descending
                                               select e.StartTime).FirstOrDefault()
                              }).ToList();

                return result;

            }catch(ApplicationException appEx)
            {
                throw appEx;
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        public int CountTotalTask(DateTime date)
        {
            int count = 0;
            try
            {
                var result = (from a in dbContext.Assignments
                              where a.AssignmentDate.Date == date.Date
                              select a.AssignmentDate).Count();
                count = result;
                return count;
            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal CountTotalInvoice(DateTime date)
        {
            decimal count = 0;
            try
            {
                var result = (from a in dbContext.Assignments
                              join b in dbContext.Invoices on a.AssignmentId equals b.AssignmentId
                              where a.AssignmentDate.Date == date.Date
                              select b.Amount).Sum();
                count = result;
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal CountTotalPayment(DateTime date)
        {
            decimal count = 0;
            try
            {
                var result = (from a in dbContext.Assignments
                              join b in dbContext.Payments on a.AssignmentId equals b.AssignmentId
                              where a.AssignmentDate.Date == date.Date
                              select b.PaymentAmount).Sum();
                count = result;
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CountTaskFailed (DateTime date)
        {
            try
            {
                var result = (from a in dbContext.Assignments
                              where a.AssignmentCode == "TASK_FAILED"
                              where a.AssignmentDate.Date == date.Date
                              select a).Count();
                
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
