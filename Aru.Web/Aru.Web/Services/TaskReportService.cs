﻿using Aru.Infrastructure.Data;
using Aru.Infrastructure.Data.Models;
using Aru.Web.Interfaces;
using Aru.Web.Models;
using Aru.Web.ViewModels;
using Microsoft.EntityFrameworkCore;
using Radyalabs.Core.Interfaces;
using Radyalabs.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.Services
{
    public class TaskReportService : ITaskReportService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly MobileForceContext dbContext;
        public TaskReportService(MobileForceContext context)
        {
            dbContext = context;
            unitOfWork = new UnitOfWork<MobileForceContext>(context);
        }

        public LatLng[] GetUserLocationActivity(string userid, DateTime dateTime)
        {
            try
            {
                var repoUserAct = unitOfWork.GetRepository<UserActivity>();
                repoUserAct.Condition = PredicateBuilder.True<UserActivity>().And(x => x.CreatedBy == userid && x.CreatedDt.Date == dateTime.Date && ( x.Latitude != 0 && x.Longitude != 0));
                repoUserAct.OrderBy = new Radyalabs.Core.Models.SqlOrderBy { Column = "CreatedDt", Type = "asc" };

                var result = repoUserAct.Find<LatLng>(x => new LatLng
                {
                    Latitude = x.Latitude,
                    Longitude = x.Longitude
                }).ToArray();

                return result;
            }
            catch (ApplicationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DetailUserReportViewModel GetDetailUserReport(string userid, DateTime dateTime)
        {
            try
            {
                var repoUser = unitOfWork.GetRepository<User>();
                repoUser.Condition = PredicateBuilder.True<User>().And(x => x.UserId == userid && x.IsDeleted == 0 && x.IsActive == 1);

                var user = repoUser.Find().FirstOrDefault();
                if (user == null)
                    throw new ApplicationException("not found");
                
                var result = (from a in dbContext.AssignmentGroups
                                join b in dbContext.Users on a.CreatedBy equals b.UserId
                                join c in dbContext.Accounts.Include(x=>x.Role) on b.AccountId equals c.AccountId
                                where b.UserId == userid && a.StartTime.Date == dateTime.Date
                                select new DetailUserReportViewModel
                                {
                                    UserCode = b.UserCode,
                                    UserId = b.UserId,
                                    Name = b.UserName,
                                    UserEmail = b.UserEmail,
                                    RoleName = c.Role.RoleName,
                                    UserName = b.UserName,
                                    UserPhone = b.UserPhone,
                                    StartLatitude = a.StartLatitude??0,
                                    EndLatitude = a.EndLatitude??0,
                                    StartLongitude = a.StartLongitude??0,
                                    EndLongitude = a.EndLongitude??0,
                                    StartTime = a.StartTime,
                                    TotalLostTime = a.TotalLostTime,
                                    EndedTime = a.EndTime,
                                    VisitHistory = (from x in dbContext.AssignmentDetails
                                                    join y in dbContext.Assignments on x.AssignmentId equals y.AssignmentId
                                                    join z in dbContext.AssignmentStatuses on y.AssignmentStatusCode equals z.AssignmentStatusCode
                                                    where x.UserId == userid && (x.EndTime != null && x.EndTime.Value.Date == a.StartTime.Date)
                                                    select new VisitHistoryViewModel
                                                    {
                                                        CustomerName = y.AssignmentName,
                                                        EndedTime = x.EndTime,
                                                        StartedTime = x.StartTime,
                                                        Status = z.AssignmentStatusName,
                                                        TaskName = y.AssignmentName,
                                                        TotalInvoice = y.Invoices.Count
                                                    }
                                                    ).ToList(),
                                    LocationHistory = (from h in dbContext.AssignmentDetails.Include(i => i.Assignment).Include(j => j.Contact)
                                                       join i in dbContext.UserActivities on h.AssignmentId equals i.AssignmentId into userActivity
                                                       from i in userActivity.DefaultIfEmpty()
                                                       where h.UserId == b.UserId && h.StartTime.Date == a.StartTime.Date                                                       select new LocationHistoryViewModel
                                                       {
                                                           Label = h.Contact.ContactName + ": " + h.Assignment.Address,
                                                           Latitude = i.Latitude,
                                                           Status = i.AssignmentStatusCode,
                                                           Longitude = i.Longitude,
                                                           StartTime = i.CreatedDt
                                                       }
                                                    ).ToList()
                                }).FirstOrDefault();

                return result;

            }
            catch(ApplicationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
