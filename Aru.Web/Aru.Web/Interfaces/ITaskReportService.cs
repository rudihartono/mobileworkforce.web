﻿using Aru.Web.Models;
using Aru.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.Interfaces
{
    public interface ITaskReportService
    {
        LatLng[] GetUserLocationActivity(string userid, DateTime dateTime);
        DetailUserReportViewModel GetDetailUserReport(string userid, DateTime dateTime);
    }
}
