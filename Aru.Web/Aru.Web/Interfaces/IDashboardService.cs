﻿using Aru.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aru.Web.Interfaces
{
    public interface IDashboardService
    {
        List<LocationHistoryViewModel> GetLocationAgent();
        //Fungsi untuk menghitung jumlah task berdasarkan waktu
        int CountTotalTask(DateTime date);
        //Fungsi untuk menghitung jumlah tagihan berdasarkan waktu
        decimal CountTotalInvoice(DateTime date);
        //Fungsi untuk menghitung jumlah pembayaran berdasarkan waktu
        decimal CountTotalPayment(DateTime date);
        //Fungsi untuk menghitung jumlah task gagal berdasarkan waktu
        int CountTaskFailed(DateTime date);
    }
}
