﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Aru.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Radyalabs.Core.Extensions;
using Radyalabs.Core.Helpers;

namespace Aru.Web.Controllers
{
    public class ContactController : Controller
    {
        private readonly ICustomerService customerService;
        public ContactController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            ViewBag.SelectedMenu = "contact";

            base.OnActionExecuted(context);
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            var userDetail = new ContactViewModel();
            var result = customerService.GetContact(id);
            if (result != null)
                CopyProperty.CopyPropertiesTo(result, userDetail);

            return View(userDetail);
        }

        public IActionResult Create()
        {
            ContactViewModel model = new ContactViewModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = Aru.Web.Helpers.AppCookieHelper.Get<UserAuthenticated>(this.HttpContext);

                    ContactModel contact = new ContactModel();
                    CopyProperty.CopyPropertiesTo(model, contact);
                    contact.ContactId = Guid.NewGuid().ToString();
                    contact.ContactNumber = model.ContactNumber.MobilePhoneFormat();
                    customerService.AddContact(contact, userAuth.UserId);

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                }
            }

            return View(model);
        }

        public IActionResult Edit(string id)
        {
            ContactViewModel contact = new ContactViewModel();

            try
            {
                if (string.IsNullOrEmpty(id))
                    return NotFound();

                var result = customerService.GetContact(id);
                CopyProperty.CopyPropertiesTo(result, contact);
            }catch(Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }

            return View(contact);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = Aru.Web.Helpers.AppCookieHelper.Get<UserAuthenticated>(this.HttpContext);

                    ContactModel contact = new ContactModel();
                    CopyProperty.CopyPropertiesTo(model, contact);
                    contact.ContactId = id;
                    contact.ContactNumber = contact.ContactNumber.MobilePhoneFormat();

                    customerService.EditContact(contact, userAuth.UserId);

                    return RedirectToAction(nameof(Index));
                }
                catch(Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = Aru.Web.Helpers.AppCookieHelper.Get<UserAuthenticated>(this.HttpContext);
                    
                    customerService.DeleteContact(id, userAuth.UserId);
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                }
            }
            
            return RedirectToAction(nameof(Index));
        }
    }
}