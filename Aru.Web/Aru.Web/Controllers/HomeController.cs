﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Aru.Business.Interfaces;
using Aru.Web.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using Aru.Web.Interfaces;
using Aru.Web.ViewModels;
using Microsoft.Extensions.Options;

namespace Aru.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IAssignmentService assignmentService;
        private readonly IDashboardService dashboardService;
        private readonly ApplicationSetting applicationSetting;
        public HomeController(IAssignmentService assignmentService, IDashboardService dashboardService, IOptions<ApplicationSetting> option)
        {
            this.assignmentService = assignmentService;
            this.dashboardService = dashboardService;
            this.applicationSetting = option.Value;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            ViewBag.SelectedMenu = "home";
            base.OnActionExecuted(context);
        }

        public IActionResult Index()
        {
            DashboardViewModel model = new DashboardViewModel();
            DateTime utcNow = DateTime.UtcNow;
            int totalKunjungan = 0;
            decimal totalInvoice = 0;
            decimal totalPaymeent = 0;
            int totalKunjunganGagal = 0;

            try
            {
                model.AgentLocation = dashboardService.GetLocationAgent();
                totalKunjungan = dashboardService.CountTotalTask(utcNow);
                totalKunjunganGagal = dashboardService.CountTaskFailed(utcNow);
                totalInvoice = dashboardService.CountTotalInvoice(utcNow);
                totalPaymeent = dashboardService.CountTotalPayment(utcNow);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }

            ViewBag.TotalKunjungan = totalKunjungan;
            ViewBag.NominalInvoice = totalInvoice;
            ViewBag.NominalPayment = totalPaymeent;
            ViewBag.KunjunganGagal = totalKunjunganGagal;
            ViewBag.GoogleMapsKey = applicationSetting.GoogleMapsKey;
            
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
