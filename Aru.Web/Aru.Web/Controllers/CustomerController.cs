﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Aru.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Radyalabs.Core.Extensions;
using Radyalabs.Core.Helpers;

namespace Aru.Web.Controllers
{
    [Authorize(Roles = "SA,OPR")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            ViewBag.SelectedMenu = "customer";

            base.OnActionExecuted(context);
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            var userDetail = new CustomerViewModel();
            var result = customerService.Get(id);
            if (result != null)
                CopyProperty.CopyPropertiesTo(result, userDetail);

            return View(userDetail);
        }

        public IActionResult Create()
        {
            CustomerViewModel model = new CustomerViewModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([FromForm]CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = Aru.Web.Helpers.AppCookieHelper.Get<UserAuthenticated>(this.HttpContext);

                    CustomerModel customer = new CustomerModel();
                    CopyProperty.CopyPropertiesTo(model, customer);

                    customer.CustomerId = Guid.NewGuid().ToString();
                    customer.CustomerCode = model.CustomerName.GetInitial();
                    
                    if (model.SaveAsContact)
                    {
                        customer.Contacts = new List<ContactModel>
                        {
                            new ContactModel
                            {
                                ContactName = model.CustomerName,
                                ContactNumber = model.CustomerPhoneNumber,
                                CustomerCode = customer.CustomerCode
                            }
                        };
                    }
                    
                    customerService.Add(customer, userAuth.UserId, model.SaveAsContact);

                    return RedirectToAction(nameof(Index));
                }
                catch(Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(string id) 
        {
            CustomerViewModel model = new CustomerViewModel();

            try
            {
                if (string.IsNullOrEmpty(id))
                    return NotFound();

                var customer = customerService.Get(id);
                if (customer == null)
                    return NotFound();

                CopyProperty.CopyPropertiesTo(customer, model);
            }catch
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = Aru.Web.Helpers.AppCookieHelper.Get<UserAuthenticated>(this.HttpContext);

                    CustomerModel customer = new CustomerModel();
                    CopyProperty.CopyPropertiesTo(model, customer);
                    customer.CustomerId = id;
                    
                    customerService.Edit(customer, userAuth.UserId);
                    return RedirectToAction(nameof(Index));
                }
                catch(Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userAuth = Aru.Web.Helpers.AppCookieHelper.Get<UserAuthenticated>(this.HttpContext);
                    customerService.Delete(id, userAuth.UserId);
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                }
            }

            return RedirectToAction(nameof(Index));
        }
    }
}