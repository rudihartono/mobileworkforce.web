﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aru.Business.Entities;
using Aru.Business.Interfaces;
using Aru.Web.Helpers;
using Aru.Web.Interfaces;
using Aru.Web.Models;
using Aru.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Aru.Web.Controllers
{
    [Authorize(Roles = "SA,OPR")]
    public class ReportController : Controller
    {
        private readonly ITaskReportService reportService;
        private readonly IAssignmentReportService assignmentReportService;
        private readonly ApplicationSetting applicationSetting;
        public ReportController(ITaskReportService reportService, 
            IOptions<ApplicationSetting> options,
            IAssignmentReportService assignmentReportService)
        {
            this.reportService = reportService;
            this.assignmentReportService = assignmentReportService;
            applicationSetting = options.Value;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            ViewBag.SelectedMenu = "report";

            base.OnActionExecuted(context);
        }

        public IActionResult Index()
        {
            ViewBag.CurrentDate = DateTime.UtcNow.ToLocalTime().ToString("MM/dd/yyyy");

            return View();
        }

        public IActionResult Driver()
        {

            ViewBag.CurrentDate = DateTime.UtcNow.ToLocalTime().ToString("MM/dd/yyyy");
            return View();
        }
        
        private double GetDistanceFromRoute(LatLng[] routes)
        {
            if (routes == null || routes.Length == 0)
                return 0;

            double distance = 0;
            int i = 0;
            MapsHelper mapsHelper = new MapsHelper();
            while (i < routes.Count())
            {
                if (i + 1 < routes.Count())
                    distance += mapsHelper.Distance(routes[i], routes[i + 1]);
                i++;
            }

            return distance/1000;
        }

        public async Task<IActionResult> NewDetail(string id, DateTime date)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            CmsDetailUserReportViewModel model = new CmsDetailUserReportViewModel();

            try
            {
                model = await assignmentReportService.GetDetail(id, date);
                if (model == null)
                    return NotFound();

                var wayRoute =  reportService.GetUserLocationActivity(id, date);
                
                model.TotalKM = GetDistanceFromRoute(wayRoute);
                model.LocationHistoryJson = string.Format("{0}", JsonConvert.SerializeObject(model.LocationHistory));
                model.VisitHistoryJson = string.Format("{0}", JsonConvert.SerializeObject(model.VisitHistory));
                model.UserWayRoutesJson = string.Format("{0}", JsonConvert.SerializeObject(wayRoute));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }

            ViewBag.GoogleMapsKey = applicationSetting.GoogleMapsKey;
            return View(model);
        }

        public async Task<IActionResult> Details(string id, DateTime date)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            CmsDetailUserReportViewModel model = new CmsDetailUserReportViewModel();

            try
            {
                model = await assignmentReportService.GetDetail(id, date);
                if (model == null)
                    return NotFound();

                var wayRoute = reportService.GetUserLocationActivity(id, date);

                model.TotalKM = GetDistanceFromRoute(wayRoute);
                model.LocationHistoryJson = string.Format("{0}", JsonConvert.SerializeObject(model.LocationHistory));
                model.VisitHistoryJson = string.Format("{0}", JsonConvert.SerializeObject(model.VisitHistory));
                model.UserWayRoutesJson = string.Format("{0}", JsonConvert.SerializeObject(wayRoute));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }

            ViewBag.GoogleMapsKey = applicationSetting.GoogleMapsKey;
            return View(model);
        }
    }
}